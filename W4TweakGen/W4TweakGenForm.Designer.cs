﻿namespace W4TweakGen
{
    partial class W4TweakGenForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(W4TweakGenForm));
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.lblGerich = new System.Windows.Forms.Label();
            this.btnMod = new System.Windows.Forms.Button();
            this.lblMod = new System.Windows.Forms.Label();
            this.lblResult = new System.Windows.Forms.Label();
            this.btnResult = new System.Windows.Forms.Button();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.txtMod = new System.Windows.Forms.TextBox();
            this.btnGen = new System.Windows.Forms.Button();
            this.chkOpenResult = new System.Windows.Forms.CheckBox();
            this.linkW4tweaks = new System.Windows.Forms.LinkLabel();
            this.imgLoading = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgLoading)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "xml";
            this.openFileDialog.Filter = "XML файлы|*.xml";
            this.openFileDialog.Title = "Файл мода";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "xml";
            this.saveFileDialog.Filter = "XML файлы|*.xml";
            this.saveFileDialog.Title = "Сохранить сгенерированный файл";
            // 
            // lblGerich
            // 
            this.lblGerich.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblGerich.ForeColor = System.Drawing.Color.LightSteelBlue;
            this.lblGerich.Location = new System.Drawing.Point(13, 84);
            this.lblGerich.Name = "lblGerich";
            this.lblGerich.Size = new System.Drawing.Size(120, 13);
            this.lblGerich.TabIndex = 0;
            this.lblGerich.Text = "© Герыч 2010";
            // 
            // btnMod
            // 
            this.btnMod.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnMod.Location = new System.Drawing.Point(472, 12);
            this.btnMod.Name = "btnMod";
            this.btnMod.Size = new System.Drawing.Size(19, 20);
            this.btnMod.TabIndex = 2;
            this.btnMod.Text = "...";
            this.btnMod.UseVisualStyleBackColor = true;
            this.btnMod.Click += new System.EventHandler(this.btnMod_Click);
            // 
            // lblMod
            // 
            this.lblMod.AutoSize = true;
            this.lblMod.Location = new System.Drawing.Point(12, 12);
            this.lblMod.Name = "lblMod";
            this.lblMod.Size = new System.Drawing.Size(28, 13);
            this.lblMod.TabIndex = 3;
            this.lblMod.Text = "Мод";
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.Location = new System.Drawing.Point(12, 38);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(59, 13);
            this.lblResult.TabIndex = 6;
            this.lblResult.Text = "Результат";
            // 
            // btnResult
            // 
            this.btnResult.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnResult.Location = new System.Drawing.Point(472, 38);
            this.btnResult.Name = "btnResult";
            this.btnResult.Size = new System.Drawing.Size(19, 20);
            this.btnResult.TabIndex = 5;
            this.btnResult.Text = "...";
            this.btnResult.UseVisualStyleBackColor = true;
            this.btnResult.Click += new System.EventHandler(this.btnResult_Click);
            // 
            // txtResult
            // 
            this.txtResult.Location = new System.Drawing.Point(77, 38);
            this.txtResult.Name = "txtResult";
            this.txtResult.ReadOnly = true;
            this.txtResult.Size = new System.Drawing.Size(389, 20);
            this.txtResult.TabIndex = 4;
            // 
            // txtMod
            // 
            this.txtMod.Location = new System.Drawing.Point(46, 12);
            this.txtMod.Name = "txtMod";
            this.txtMod.ReadOnly = true;
            this.txtMod.Size = new System.Drawing.Size(420, 20);
            this.txtMod.TabIndex = 1;
            // 
            // btnGen
            // 
            this.btnGen.Location = new System.Drawing.Point(347, 64);
            this.btnGen.Name = "btnGen";
            this.btnGen.Size = new System.Drawing.Size(144, 30);
            this.btnGen.TabIndex = 7;
            this.btnGen.Text = "Создать";
            this.btnGen.UseVisualStyleBackColor = true;
            this.btnGen.Click += new System.EventHandler(this.btnGen_Click);
            // 
            // chkOpenResult
            // 
            this.chkOpenResult.AutoSize = true;
            this.chkOpenResult.Location = new System.Drawing.Point(15, 64);
            this.chkOpenResult.Name = "chkOpenResult";
            this.chkOpenResult.Size = new System.Drawing.Size(246, 17);
            this.chkOpenResult.TabIndex = 8;
            this.chkOpenResult.Text = "Открыть результат в текстовом редакторе";
            this.chkOpenResult.UseVisualStyleBackColor = true;
            // 
            // linkW4tweaks
            // 
            this.linkW4tweaks.ActiveLinkColor = System.Drawing.Color.OrangeRed;
            this.linkW4tweaks.AutoSize = true;
            this.linkW4tweaks.Cursor = System.Windows.Forms.Cursors.Hand;
            this.linkW4tweaks.DisabledLinkColor = System.Drawing.Color.LightSteelBlue;
            this.linkW4tweaks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.linkW4tweaks.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.linkW4tweaks.LinkColor = System.Drawing.Color.LightSteelBlue;
            this.linkW4tweaks.Location = new System.Drawing.Point(112, 84);
            this.linkW4tweaks.Name = "linkW4tweaks";
            this.linkW4tweaks.Size = new System.Drawing.Size(78, 13);
            this.linkW4tweaks.TabIndex = 9;
            this.linkW4tweaks.TabStop = true;
            this.linkW4tweaks.Text = "w4tweaks.ru";
            this.linkW4tweaks.VisitedLinkColor = System.Drawing.Color.LightSteelBlue;
            this.linkW4tweaks.MouseLeave += new System.EventHandler(this.linkW4tweaks_MouseLeave);
            this.linkW4tweaks.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkW4tweaks_LinkClicked);
            this.linkW4tweaks.MouseEnter += new System.EventHandler(this.linkW4tweaks_MouseEnter);
            // 
            // imgLoading
            // 
            this.imgLoading.ErrorImage = null;
            this.imgLoading.Image = ((System.Drawing.Image)(resources.GetObject("imgLoading.Image")));
            this.imgLoading.InitialImage = null;
            this.imgLoading.Location = new System.Drawing.Point(309, 64);
            this.imgLoading.Name = "imgLoading";
            this.imgLoading.Size = new System.Drawing.Size(32, 32);
            this.imgLoading.TabIndex = 10;
            this.imgLoading.TabStop = false;
            this.imgLoading.Visible = false;
            // 
            // W4TweakGenForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(503, 101);
            this.Controls.Add(this.imgLoading);
            this.Controls.Add(this.linkW4tweaks);
            this.Controls.Add(this.chkOpenResult);
            this.Controls.Add(this.btnGen);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.btnResult);
            this.Controls.Add(this.txtResult);
            this.Controls.Add(this.lblMod);
            this.Controls.Add(this.btnMod);
            this.Controls.Add(this.txtMod);
            this.Controls.Add(this.lblGerich);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "W4TweakGenForm";
            this.Text = "W4TweakGen";
            this.Load += new System.EventHandler(this.W4TweakGenForm_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.W4TweakGenForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.imgLoading)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.Label lblGerich;
        private System.Windows.Forms.Button btnMod;
        private System.Windows.Forms.Label lblMod;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.Button btnResult;
        private System.Windows.Forms.TextBox txtResult;
        private System.Windows.Forms.TextBox txtMod;
        private System.Windows.Forms.Button btnGen;
        private System.Windows.Forms.CheckBox chkOpenResult;
        private System.Windows.Forms.LinkLabel linkW4tweaks;
        private System.Windows.Forms.PictureBox imgLoading;

    }
}

