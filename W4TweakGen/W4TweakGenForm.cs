﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Diagnostics;
using System.Threading;
using System.IO;

namespace W4TweakGen
{
    public partial class W4TweakGenForm : Form
    {
        public const string RegistryKey = @"HKEY_LOCAL_MACHINE\SOFTWARE\Gerich\W4TweakGen\v0.1";

        string modDir;
        string resultDir;
        bool allFilled;
        bool busy;
        Thread worker = null;

        public W4TweakGenForm()
        {
            InitializeComponent();
        }

        private void W4TweakGenForm_Load(object sender, EventArgs e)
        {
            string savedModPath = Registry.GetValue(RegistryKey, "modpath", "") as string;
            string savedResultPath = Registry.GetValue(RegistryKey, "resultpath", "") as string;
            modDir = Registry.GetValue(RegistryKey, "moddir", "") as string;
            resultDir = Registry.GetValue(RegistryKey, "resultdir", "") as string;
            bool openResult = Convert.ToBoolean(Registry.GetValue(RegistryKey, "openresult", false));

            if ((modDir == "") || (resultDir == ""))
            {
                string startupPath = Application.StartupPath;
                if (startupPath[startupPath.Length - 1] != '\\')
                    startupPath += "\\";
                if (modDir == "")
                    modDir = startupPath + "samples";
                if (resultDir == "")
                    resultDir = startupPath + "result";
            }

            saveFileDialog.InitialDirectory = resultDir;
            openFileDialog.InitialDirectory = modDir;

            if (savedModPath != "")
            {
                openFileDialog.FileName = savedModPath;
                txtMod.Text = savedModPath;
            }

            if (savedResultPath != "")
            {
                saveFileDialog.FileName = savedResultPath;
                txtResult.Text = savedResultPath;
            }

            chkOpenResult.Checked = openResult;

            allFilled = (savedModPath != "") && (savedResultPath != "");
            busy = false;
            enableGen();
        }

        private void linkW4tweaks_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://w4tweaks.ru/");
        }

        private void linkW4tweaks_MouseLeave(object sender, EventArgs e)
        {
            linkW4tweaks.LinkColor = Color.LightSteelBlue;
        }

        private void linkW4tweaks_MouseEnter(object sender, EventArgs e)
        {
            linkW4tweaks.LinkColor = Color.OrangeRed;
        }

        private void W4TweakGenForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Registry.SetValue(RegistryKey, "modpath", txtMod.Text);
            Registry.SetValue(RegistryKey, "resultpath", txtResult.Text);
            Registry.SetValue(RegistryKey, "moddir", openFileDialog.InitialDirectory);
            Registry.SetValue(RegistryKey, "resultdir", saveFileDialog.InitialDirectory);
            Registry.SetValue(RegistryKey, "openresult", chkOpenResult.Checked);
            if (worker != null)
                if (worker.IsAlive)
                    try
                    {
                        worker.Abort();
                    }
                    catch
                    {
                    }
        }

        private void btnMod_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                txtMod.Text = openFileDialog.FileName;
                allFilled = (txtResult.Text != "");
                enableGen();
            }
        }

        private void btnResult_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                txtResult.Text = saveFileDialog.FileName;
                allFilled = (txtMod.Text != "");
                enableGen();
            }
        }

        private void btnGen_Click(object sender, EventArgs e)
        {
            busy = true;
            enableGen();
            bool openResult = chkOpenResult.Checked;
            string mod = txtMod.Text;
            string result = txtResult.Text;
            string args = @"-s:""" + mod + @""" -xsl:W4TweakGenerator.xsl -o:""" + result + @"""";
            worker = new Thread(delegate()
            {
                try
                {
                    Directory.SetCurrentDirectory(Application.StartupPath);
                    Process Transform = new Process();

                    Transform.StartInfo.FileName = @"saxon\Transform.exe";
                    Transform.StartInfo.Arguments = args;
                    Transform.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    try
                    {
                        Transform.Start();

                        if (Transform.WaitForExit(1000 * 60 * 2))
                        {
                            if (openResult)
                                try
                                {
                                    Process.Start(result);
                                }
                                catch
                                {
                                    MessageBox.Show("Результат не найден!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                }
                        }
                        else
                            if (MessageBox.Show("Увы, но что-то не так.\nГенератор висит около 2-х минут.\nЖдём ещё 2 минуты?", "Висак =(", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                            {
                                while (!Transform.WaitForExit(1000 * 60 * 2))
                                    if (MessageBox.Show("Генератор всё ещё висит\nЕщё 2 минуты?", "Всё ещё висак =(", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.No)
                                    {
                                        try { Transform.Kill(); }
                                        catch { }
                                        busy = false;
                                        enableGen();
                                        return;
                                    }

                                if (openResult)
                                    try
                                    {
                                        Process.Start(result);
                                    }
                                    catch
                                    {
                                        MessageBox.Show("Результат не найден!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                    }
                            }
                            else
                                try { Transform.Kill(); }
                                catch { }
                    }
                    catch
                    {
                        try { Transform.Kill(); }
                        catch { }
                    }
                }
                catch (FileNotFoundException)
                {
                    MessageBox.Show("Скорее всего не найден файл saxon\\Transform.exe в папке с W4TweakGen\nГенерация мода провалилась", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch
                {

                }
                busy = false;
                enableGen();
            });
            worker.Start();
        }

        private void enableGen()
        {
            btnGen.Invoke(new MethodInvoker(delegate()
                {
                    btnGen.Enabled = allFilled && !busy;
                    imgLoading.Visible = busy;
                }));
        }
    }
}
